package chip8;

import com.sun.prism.impl.VertexBuffer;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11.*;
import org.lwjgl.system.Callback;
import org.lwjgl.system.CallbackI;
import org.lwjgl.system.MemoryUtil;

import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Chip8Emu {
    // Window handle
    private static long window;

    private void windowInit() {
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize window system
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        // Create window
        window = glfwCreateWindow(640, 480, "Chip-8 Emulator", NULL, NULL);

        // Check for successful window creation
        if (window == NULL)
            throw new RuntimeException("Failed to create window");

        glfwMakeContextCurrent(window);
        glfwSwapInterval(1);
        glfwShowWindow(window);
    }

    private void run() {
        // Initialize the system
        Chip8VirtualMachine c8vm = new Chip8VirtualMachine();

        try {
            // Load a ROM
            c8vm.loadGame(new File("roms/pong.rom"));
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load ROM");
        }

        while(true) {
            c8vm.emulateCycle();
            if (c8vm.getDrawFlag())
                c8vm.drawScreenDebug();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                // Do Nothing
            }
        }
    }

    public static void main(String[] args) {
        Chip8Emu c8emu = new Chip8Emu();
        // TODO Set up graphics

        c8emu.run();
    }
}
