package chip8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class Chip8VirtualMachine {
    private static final boolean DEBUG = false;

    /**
     * 4096 bytes of memory
     */
    private char[] memory;

    private char[] register;

    /**
     * Program counter
     */
    private short programCounter;

    /**
     * Current opcode
     */
    private short opcode;

    /**
     * Index register
     */
    private short indexRegister;

    /**
     * Stack pointer
     */
    private short stackPointer;

    /**
     * Timers
     */
    private char delayTimer;
    private char soundTimer;

    /**
     * Graphics array
     */
    private char[] graphicsArray;
    private boolean drawFlag;
    private char[] stack;

    public Chip8VirtualMachine() {
        // Reset the machine
        memory = new char[4096];
        programCounter = 0x200;
        opcode = 0;
        indexRegister = 0;
        stackPointer = 0;
        register = new char[16];
        graphicsArray = new char[2048];
        drawFlag = false;
        stack = new char[16];

        for (char c : graphicsArray) {
            c = 0x0;
        }

        char chip8FontSet[] = {
                0xF0, 0x90, 0x90, 0x90, 0xF0,   // 0
                0x20, 0x60, 0x20, 0x20, 0x70,   // 1
                0xF0, 0x10, 0xF0, 0x80, 0xF0,   // 2
                0xF0, 0x10, 0xF0, 0x10, 0xF0,   // 3
                0x90, 0x90, 0xF0, 0x10, 0x10,   // 4
                0xF0, 0x80, 0xF0, 0x10, 0xF0,   // 5
                0xF0, 0x80, 0xF0, 0x90, 0xF0,   // 6
                0xF0, 0x10, 0x20, 0x40, 0x40,   // 7
                0xF0, 0x90, 0xF0, 0x90, 0xF0,   // 8
                0xF0, 0x90, 0xF0, 0x10, 0xF0,   // 9
                0xF0, 0x90, 0xF0, 0x90, 0x90,   // A
                0xE0, 0x90, 0xE0, 0x90, 0xE0,   // B
                0xF0, 0x80, 0x80, 0x80, 0xF0,   // C
                0xE0, 0x90, 0x90, 0x90, 0xE0,   // D
                0xF0, 0x80, 0xF0, 0x80, 0xF0,   // E
                0xF0, 0x80, 0xF0, 0x80, 0x80    // F
        };

        // Load fontset
        for (int i = 0; i < 80; i++) {
            memory[i] = chip8FontSet[i];
        }

        // Reset Timers
        delayTimer = 0;
        soundTimer = 0;
    }

    public void loadGame(File game) throws IOException {
        // Open file
        FileInputStream fReader = new FileInputStream(game);

        // Load the ROM into memory starting at 0x200 (512)
        int counter = 512;
        int in;
        while ((in = fReader.read()) != -1) {
            memory[counter++] = (char) in;
        }
    }

    public char getValueAtPos(int x) {
        return memory[x];
    }

    public void emulateCycle() {
        // Fetch opcode
        opcode = (short) (memory[programCounter] << 8 | memory[programCounter + 1]);

        // Decode opcode
        processOpcode(opcode);

        // Update times
        if (delayTimer > 0)
            delayTimer--;

        if (soundTimer > 0) {
            if (soundTimer == 1)
                System.out.println("BEEP");

            soundTimer--;
        }
    }

    public void processOpcode(short opcode) {
//        System.out.print("0x" + String.format("%x", (int) (opcode & 0x0000FFFF)) + ": ");

        switch (opcode & 0xF000) {
            case 0x0000:
                switch(opcode & 0x00FF) {
                    case 0x00E0:    // Clear the screen
                        for (char c : graphicsArray) {
                            c = 0;
                        }
                        programCounter += 2;
                        break;

                    case 0x00EE:    // Return from subroutine
                        programCounter = (short) stack[--stackPointer];
                        programCounter += 2;
                        break;

                    default:
                        System.err.println("0x" + String.format("%04x", (int) (opcode & 0x0000FFFF)) + ": " + "Unknown opcode [0x" + String.format("%04x", (int) (opcode & 0xF000)) + "]");
                }
                break;

            case 0x1000:    // Goto address
                programCounter = (short) (opcode & 0x0FFF);
                break;

            case 0x2000:    // Execute subroutine
                stack[stackPointer] = (char) programCounter; // Store return point
                stackPointer++;
                programCounter = (short) (opcode & 0x0FFF);

                debug("Execute subroutine at memory location " + (int) (opcode & 0x0FFF));
                break;

            case 0x3000:    // Skip on condition
                if (register[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
                    programCounter += 4;
                else
                    programCounter += 2;
                break;

            case 0x4000:    // Skip on negative condition
                if (register[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
                    programCounter += 4;
                else
                    programCounter += 2;
                break;

            case 0x5000:    // Skip on condition registers
                if (register[(opcode & 0x0F00) >> 8] == register[(opcode & 0x00F0) >> 4])
                    programCounter += 4;
                else
                    programCounter += 2;
                break;

            case 0x6000:    // Store number in register
                debug("Store " + (opcode & 0x00FF) + " in register V" + ((opcode & 0x0F00) >> 8));
                register[(opcode & 0x0F00) >> 8] = (char) (opcode & 0x00FF);
                programCounter += 2;
                break;

            case 0x7000:    // Add constant to register
                char newVal = (char) (register[(opcode & 0x0F00) >> 8] + (opcode & 0x00FF));

                // Check for overflow
                if (newVal > 0xFF) {
                    register[(opcode & 0x0F00) >> 8] = (char) (newVal - 0xFF);
                } else {
                    register[(opcode & 0x0F00) >> 8] = newVal;
                }

                programCounter += 2;
                break;

            case 0x8000:    // Assignments & BitOps
                int temp = 0;

                switch(opcode & 0x000F) {
                    case 0x0000:    // Assign VX to value of VY
                        register[(opcode & 0x0F00) >> 8] = register[(opcode & 0x00F0) >> 4];
                        programCounter +=2;
                        break;

                    case 0x0001:    // Assign VX to VX | VY
                        register[(opcode & 0x0F00) >> 8] |= register[(opcode & 0x00F0) >> 4];
                        programCounter += 2;
                        break;

                    case 0x0002:    // Assign VX to VX & VY
                        register[(opcode & 0x0F00) >> 8] &= register[(opcode & 0x00F0) >> 4];
                        programCounter += 2;
                        break;

                    case 0x0003:    // Assign VX to VX ^ VY
                        register[(opcode & 0x0F00) >> 8] ^= register[(opcode & 0x00F0) >> 4];
                        programCounter += 2;
                        break;

                    case 0x0004:    // Assign VX to VX + VY
                        // Add registers together
                        temp = register[(opcode & 0x0F00) >> 8] + register[(opcode & 0x00F0) >> 4];

                        // Find carry
                        if ((temp - 0xFF) > 0 ) {   // Set carry
                            register[0xF] = 1;
                            register[(opcode & 0x0F00) >> 8] = (char) (temp - 0xFF);
                        } else {
                            register[(opcode & 0x0F00) >> 8] += register[(opcode & 0x00F0) >> 4];
                        }

                        programCounter += 2;
                        break;

                    case 0x0005:    // Assign VX to VX - VY
                        // Add registers together
                        temp = register[(opcode & 0x0F00) >> 8] - register[(opcode & 0x00F0) >> 4];

                        // Find carry
                        if (register[(opcode & 0x0F00) >> 8] > register[(opcode & 0x00F0) >> 4]) {
                            register[0xF] = 1;
                            register[(opcode & 0x0F00) >> 8] = (char) temp;
                        } else {
                            register[0xF] = 0;
                            register[(opcode & 0x0F00) >> 8] = (char) (0xFF + temp);
                        }

                        programCounter += 2;
                        break;

                    case 0x0006:
                        // Move least significant bit to VF
                        register[0xF] = (char) (register[(opcode & 0x0F00 >> 8)] & 0x000F);

                        register[(opcode & 0x0F00 >> 8)] = (char) (register[(opcode * 0x00F0) >> 5] >> 1);

                        // TODO Check this opcode. It might not be done.
                        programCounter += 2;
                        break;

                    case 0x0007:    // Subtract
                        // Check for carry
                        if (register[opcode & 0x00F0] > register[opcode & 0x0F00])
                            register[0xF] = 1;
                        else
                            register[0xF] = 0;

                        // Subtract Vy - Vx; store in Vx
                        register[opcode & 0x0F00] = (char) (register[opcode & 0x00F0] - register[opcode & 0x0F00]);

                        programCounter += 2;
                        break;

                    default:
                        System.err.println("Unknown opcode");
                        programCounter += 2;
                        break;
                }
                break;

            case 0x9000:
                if (register[(opcode & 0x0F00) >> 8] != register[(opcode & 0x00F0) >> 4])
                    programCounter += 4;
                else
                    programCounter += 2;
                break;

            case 0xA000:    // Store memory address in index register
                debug("Store memory address " + (opcode & 0x0FFF) + " in index register");
                indexRegister = (short) (opcode & 0x0FFF);
                programCounter += 2;
                break;

            case 0xB000:    // Jump to memory address
                programCounter = (short) (register[0] + (opcode & 0x0FFF));
                break;

            case 0xC000:    // VX to Rand & Num
                Random rand = new Random(System.currentTimeMillis());
                register[(opcode & 0x0F00) >> 8] = (char) (rand.nextInt(255) & (opcode & 0x00FF));
                programCounter += 2;
                break;

            case 0xD000:    // Draw sprite to screen
                char x = register[(opcode & 0x0F00) >> 8];  // X Coord
                char y = register[(opcode & 0x00F0) >> 4];  // Y Coord
                char height = (char) (opcode & 0x000F);
                char pixel;

                // Reset register VF
                register[0xF] = 0;

                for ( int yline = 0; yline < height; yline++) {
                    pixel = memory[indexRegister + yline];  // Get the byte stored in this memory address
                    for (int xline = 0; xline < 8; xline++) {   // Move across the line
                        if ((pixel & (0x80 >> xline)) != 0) {
                            if(graphicsArray[x + xline + ((y + yline) * 64)] == 1)
                                register[0xF] = 1;
                            graphicsArray[x + xline + ((y + yline) * 64)] ^= 1;
                        }
                    }
                }

                drawFlag = true;

                debug("Draw sprite at (" + (int)x + ", " + (int)y + ")");

                programCounter += 2;
                break;

            case 0xF000:    // Multiple opcodes starting with 0xF000
                switch(opcode & 0x00FF) {
                    case 0x0007:    // Set register to timer value
                        register[(opcode & 0x0F00) >> 8] = delayTimer;
                        programCounter += 2;
                        break;

                    case 0x0015:    // Set timer to register value
                        delayTimer = register[(opcode & 0x0F00) >> 8];
                        programCounter += 2;
                        break;

                    case 0x01E:     // Add I and Vx
                        indexRegister += register[opcode & 0x0F00];
                        programCounter += 2;
                        break;

                    case 0x0033:    // Store binary-coded decimal of value
                        memory[indexRegister] = (char) (register[(opcode & 0x0F00) >> 8] / 100);
                        memory[indexRegister + 1] = (char) ((register[(opcode & 0x0F00) >> 8] / 10) % 10);
                        memory[indexRegister + 2] = (char) ((register[(opcode & 0x0F00) >> 8] % 100) % 10);
                        programCounter += 2;
                        debug("Store binary-coded decimal value");
                        break;

                    case 0x0065:    // Fill registers with values from memory
                        for (int i = 0; i <= (opcode & 0x0F00 >> 8); i++) {
                            register[i] = memory[indexRegister + i];
                        }

                        indexRegister = (short) (indexRegister + (opcode & 0x0F00 >> 8) + 1);
                        programCounter += 2;
                        break;

                    case 0x0029:    // Set index register to character in memory
                        char character = register[opcode & 0x0F00 >> 8]; // Retrieve character from register
                        switch(character) {
                            case 0:
                                indexRegister = 0x0000;
                                break;

                            case 1:
                                indexRegister = 0x0005;
                                break;

                            case 2:
                                indexRegister = 0x000a;
                                break;

                            case 3:
                                indexRegister = 0x000f;
                                break;

                            case 4:
                                indexRegister = 0x0014;
                                break;

                            case 5:
                                indexRegister = 0x0019;
                                break;

                            case 6:
                                indexRegister = 0x001e;
                                break;

                            case 7:
                                indexRegister = 0x0023;
                                break;

                            case 8:
                                indexRegister = 0x0028;
                                break;

                            case 9:
                                indexRegister = 0x002e;
                                break;

                            case 10:
                                indexRegister = 0x0033;
                                break;

                            case 11:
                                indexRegister = 0x0038;
                                break;

                            case 12:
                                indexRegister = 0x003d;
                                break;

                            case 13:
                                indexRegister = 0x0042;
                                break;

                            case 14:
                                indexRegister = 0x0047;
                                break;

                            case 15:
                                indexRegister = 0x004c;
                                break;

                            default:
                                System.err.println("Unknown character");
                                break;
                        }
                        programCounter += 2;
                        break;

                    default:
                        System.err.println("0x" + String.format("%04x", (int) (opcode & 0x0000FFFF)) + ": " + "Unknown opcode [0x" + String.format("%04x", (int) (opcode & 0xF000)) + "]");
                        programCounter += 2;
                }
                break;

            default:
                System.err.println("0x" + String.format("%04x", (int) (opcode & 0x0000FFFF)) + ": " + "Unknown opcode [0x" + String.format("%04x", (int) (opcode & 0xF000)) + "]");
                programCounter += 2;
        }
    }

    private void debug(String output) {
        if (DEBUG) {
            System.out.println(output);
        }
    }

    public void dumpMemory() {
        for (int i = 0; i < memory.length; i++) {
            System.out.print(String.format("%04x", (int) memory[i]) + " ");

            if ((i + 1) % 16 == 0)
                System.out.print("\n");
        }
    }

    public boolean getDrawFlag() {
        return this.drawFlag;
    }

    public void drawScreenDebug() {
        StringBuffer sb = new StringBuffer();
        sb.append("================================================================\n");

        for (int i = 0; i < 32; i++) {
            for (int n = i * 64; n < (i + 1) * 64; n++) {
                if (graphicsArray[n] == 1)
                    sb.append("X");
                else
                    sb.append(" ");
            }
            sb.append("\n");
        }

        sb.append("================================================================\n");

        System.out.println(sb.toString());

        this.drawFlag = false;
    }
}
