package chip8;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Random;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(PowerMockRunner.class)
public class Chip8VirtualMachineTest {
    File testRom;
    Chip8VirtualMachine cvm;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setUp() throws Exception {
        // Create new file
        testRom = folder.newFile();

        // Create output stream
        OutputStream oStream = new FileOutputStream(testRom);

        oStream.write(0x00);
        oStream.write(0x01);
        oStream.write(0x02);
        oStream.write(0x03);
        oStream.write(0x04);

        oStream.write(0xa2);
        oStream.write(162);

        oStream.close();

        cvm = new Chip8VirtualMachine();
    }

    @Test
    public void loadGame() throws Exception {
        // Load the ROM into memory
        cvm.loadGame(testRom);

        assertEquals(0x00, cvm.getValueAtPos(0x200));
        assertEquals(0x01, cvm.getValueAtPos(0x201));
        assertEquals(0x02, cvm.getValueAtPos(0x202));
        assertEquals(0x03, cvm.getValueAtPos(0x203));
        assertEquals(0x04, cvm.getValueAtPos(0x204));
        assertEquals(0xa2, cvm.getValueAtPos(0x205));
        assertEquals(0xa2, cvm.getValueAtPos(0x206));
    }

    @Test   //0x00E0
    public void testOpcodesClearScreen() throws Exception {
        Random rand = new Random();

        // Fill a display array at random
        char[] graphicsFilled = new char[2048];
        for (char c : graphicsFilled) {
            c = (char) rand.nextInt(1);
            System.out.print(c);
        }

        // Set the internal display state
        Whitebox.setInternalState(cvm, "graphicsArray", graphicsFilled);

        assertEquals(graphicsFilled, Whitebox.getInternalState(cvm, "graphicsArray"));

        // Clear the screen
        cvm.processOpcode((short) 0x00E0);

        assertArrayEquals(new char[2048], (char[]) Whitebox.getInternalState(cvm, "graphicsArray"));
    }

    @Test   // 0x00EE
    public void testOpcodesReturnFromSubroutine() throws Exception {
        // Get the internal program counter
        short programCounter = Whitebox.getInternalState(cvm, "programCounter");

        // Get the internal stack
        char[] stack = Whitebox.getInternalState(cvm, "stack");

        // Set the inter stack pointer
        Whitebox.setInternalState(cvm, "stackPointer", (short) 1);

        stack[0] = 0x555;

        // Assert we have a blank machine
        assertEquals(0x200, programCounter);

        // Test 0x00EE
        cvm.processOpcode((short) 0x00EE);

        // Assert we returned from subroutine
        // Stack has been populated with return address 0x555
        programCounter = Whitebox.getInternalState(cvm, "programCounter");
        assertEquals(0x557, programCounter);
    }

    @Test   // 0x1NNN
    public void testOpcodesJumpToAddress() throws Exception {
        // Get the internal program counter
        short programCounter = getProgramCounter();

        // Test 0x1NNN
        cvm.processOpcode((short) 0x189A);

        // Get new program counter
        short programCounterAfter = getProgramCounter();

        // Test they've changed correctly
        assertNotEquals(programCounter, programCounterAfter);
        assertEquals((short) 0x89A, programCounterAfter);
    }

    @Test   // 0x2NNN
    public void testCallSubroutineAtNNN() throws Exception {
        // Get the program counter and stack pointer
        short programCounterBefore = getProgramCounter();
        short stackPointerBefore = getStackPointer();

        // Run opcode
        cvm.processOpcode((short) 0x2ABC);

        // Get new program counter and stack pointer
        short programCounterAfter = getProgramCounter();
        short stackPointerAfter = getStackPointer();

        // Test
        assertNotEquals(programCounterBefore, programCounterAfter);
        assertEquals((short) 0xABC, programCounterAfter);

        assertNotEquals(stackPointerBefore, stackPointerAfter);
        assertEquals(stackPointerBefore + 1, stackPointerAfter);
    }

    @Test   // 0x3XKK
    public void testVXEqualsKK() {
        // Set up machine
        Whitebox.setInternalState(cvm, "programCounter", (short) 0x200);
        char[] registers = new char[16];
        registers[3] = 0xFA;
        Whitebox.setInternalState(cvm, "register", registers);

        // Run opcode - true
        cvm.processOpcode((short) 0x33FA);

        // Test positive
        assertEquals((short) 0x204, getProgramCounter());

        // Run opcode - false
        cvm.processOpcode((short) 0x33FB);

        // Test negative
        assertEquals((short) 0x206, getProgramCounter());
    }

    @Test   // 0x4XKK
    public void testVXNotEqualsKK() {
        // Set up machine
        Whitebox.setInternalState(cvm, "programCounter", (short) 0x200);
        char[] registers = new char[16];
        registers[3] = 0xFA;
        Whitebox.setInternalState(cvm, "register", registers);

        // Run opcode - true
        cvm.processOpcode((short) 0x43FB);

        // Test positive
        assertEquals((short) 0x204, getProgramCounter());

        // Run opcode - false
        cvm.processOpcode((short) 0x43FA);

        // Test negative
        assertEquals((short) 0x206, getProgramCounter());
    }

    @Test   // 0x5XY0
    public void testVXEqualsVY() {
        // Set up machine
        Whitebox.setInternalState(cvm, "programCounter", (short) 0x200);
        char[] registers = new char[16];
        registers[3] = 0xFA;
        registers[10] = 0xFA;
        Whitebox.setInternalState(cvm, "register", registers);

        // Run opcode - true
        cvm.processOpcode((short) 0x53A0);

        // Test positive
        assertEquals((short) 0x204, getProgramCounter());

        // Update registers
        registers = new char[16];
        registers[3] = 0xFA;
        registers[10] = 0x01;
        Whitebox.setInternalState(cvm, "register", registers);

        // Run opcode - false
        cvm.processOpcode((short) 0x53A0);

        // Test negative
        assertEquals((short) 0x206, getProgramCounter());
    }

    @Test
    public void testStoreValueInRegister() {
        // Set up machine
        Whitebox.setInternalState(cvm, "programCounter", (short) 0x200);
        char[] registers = new char[16];
        registers[2] = 0xFF;
        Whitebox.setInternalState(cvm, "register", registers);

        // Run opcode
        cvm.processOpcode((short) 0x62D2);

        char[] registersAfter = getRegisters();
        assertEquals(0xD2, registersAfter[2]);
    }

    @Test
    public void testOpcodesAddValue() {
        // Set up machine
        Whitebox.setInternalState(cvm, "programCounter", (short) 0x200);
        char[] registers = new char[16];
        registers[2] = 0xF0;
        Whitebox.setInternalState(cvm, "register", registers);

        // Run opcode
        cvm.processOpcode((short) 0x7202);
        assertEquals(0xF2, getRegisters()[2]);

        // Run another code
        cvm.processOpcode((short) 0x720F);
        assertEquals(0x02, getRegisters()[2]);
    }

    @Test   // 0x8XY0
    public void testOpcodesAssignVXtoVY() throws Exception {
        char vx = 0x020;
        char vy = 0x030;

        // Assign registers
        char[] registers = new char[16];
        registers[3] = vx;
        registers[10] = vy;
        Whitebox.setInternalState(cvm, "register", registers);

        // Get program counter before
        short pcBefore = getProgramCounter();

        // Run opcode & get internal registers
        cvm.processOpcode((short) 0x83a0);

        // Assert copy
        assertEquals(getRegisters()[0x3], getRegisters()[0xa]);
        assertEquals(pcBefore + 2, getProgramCounter());
    }

    @Test   // 0x8XY1
    public void testOpcodesBitwiseVXorVY() throws Exception {
        char vx = 0x5;
        char vy = 0x3;

        // Assign registers
        char[] registers = new char[16];
        registers[3] = vx;
        registers[10] = vy;
        Whitebox.setInternalState(cvm, "register", registers);

        // Get program counter before
        short pcBefore = getProgramCounter();

        // Run opcode and get state
        cvm.processOpcode((short) 0x83a1);

        // Check operation
        assertEquals(0x7, getRegisters()[0x3]);
        assertEquals(pcBefore + 2, getProgramCounter());
    }

    @Test   // 0x8XY2
    public void testOpcodesBitwiseVXandVY() throws Exception {
        char vx = 0x055;
        char vy = 0x0F0;

        // Assign registers
        char[] registers = new char[16];
        registers[3] = vx;
        registers[10] = vy;
        Whitebox.setInternalState(cvm, "register", registers);

        // Get program counter before
        short pcBefore = getProgramCounter();

        // Run opcode and get state
        cvm.processOpcode((short) 0x83a2);

        // Check operation
        assertEquals(0x050, getRegisters()[0x3]);
        assertEquals(pcBefore + 2, getProgramCounter());
    }

    @Test   // 0x8XY4
    public void testObcodeVXaddVY() {
        char vx = 0x055;
        char vy = 0x0F0;
        char vz = 0x011;

        // Assign registers
        char[] registers = new char[16];
        registers[3] = vx;
        registers[10] = vy;
        registers[5] = vz;
        Whitebox.setInternalState(cvm, "register", registers);

        // Get program counter before
        short pcBefore = getProgramCounter();

        cvm.processOpcode((short) 0x8354);

        // Test non carry
        assertEquals(0x66, getRegisters()[3]);
        assertEquals(0, getRegisters()[0xF]);
        assertEquals(pcBefore + 2, getProgramCounter());

        // Test carry
        cvm.processOpcode((short) 0x83a4);
        assertEquals(0x57, getRegisters()[3]);
        assertEquals(1, getRegisters()[0xF]);
        assertEquals(pcBefore + 4, getProgramCounter());
    }

    @Test   // 0x8XY5
    public void testObcodeVXsubVY() {
        char vx = 0x01;
        char vy = 0x03;
        char vz = 0x02;

        // Assign registers
        char[] registers = new char[16];
        registers[3] = vx;
        registers[10] = vy;
        registers[5] = vz;
        Whitebox.setInternalState(cvm, "register", registers);

        // Get program counter before
        short pcBefore = getProgramCounter();

        cvm.processOpcode((short) 0x83a5);

        // Test non carry
        assertEquals(0xFD, getRegisters()[3]);
        assertEquals(0, getRegisters()[0xF]);
        assertEquals(pcBefore + 2, getProgramCounter());

        // Test carry
        cvm.processOpcode((short) 0x8a55);
        assertEquals(0x01, getRegisters()[0xA]);
        assertEquals(1, getRegisters()[0xF]);
        assertEquals(pcBefore + 4, getProgramCounter());
    }

    private char[] getRegisters() {
        return Whitebox.getInternalState(cvm, "register");
    }

    private short getProgramCounter() {
        return Whitebox.getInternalState(cvm, "programCounter");
    }

    private short getStackPointer() {
        return Whitebox.getInternalState(cvm, "stackPointer");
    }
}